<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Confirm Registration (Ramirez)</title>
	</head>
	
	<body>
		<h1> Registration Confirmation </h1>
		
		<%
			String firstname = session.getAttribute("firstname").toString();
			String lastname = session.getAttribute("lastname").toString();
			String phonenumber = session.getAttribute("phonenumber").toString();
			String emailaddress = session.getAttribute("emailaddress").toString();
			String discover = session.getAttribute("discover").toString();
			String birthdate = session.getAttribute("birthdate").toString();
			String usertype = session.getAttribute("usertype").toString();
			String description = session.getAttribute("description").toString();
		
			if(discover.equals("friends"))
				discover = "Friends";
			else if(discover.equals("socialmedia"))
				discover = "Social Media";
			else
				discover = "Others";					
		%>
		
		<p> First Name: <b> <%= firstname %> </b> </p>
		<p> Last Name: <b> <%= lastname %> </b> </p>
		<p> Phone Number: <b> <%= phonenumber %> </b> </p>
		<p> Email Address: <b> <%= emailaddress %> </b> </p>
		<p> Application Discovery: <b> <%= discover %> </b> </p>
		<p> Date of Birth: <b> <%= birthdate %> </b> </p>
		<p> User Type: <b> <%= usertype %> </b> </p>
		<p> Description: <b> <%= description %> </b> </p> <br>
	
		<form action="login" method="post" style="display: inline-block">
				<input type="submit"> 
		</form>	&nbsp; &nbsp; 
		
		<form action="index.jsp" method="post" style="display: inline-block">
			<input type="submit" value="Back">
		</form>
		
	</body>
</html>