<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home Page (Ramirez)</title>
	</head>
		
	<body>
		
		<%
			String name = session.getAttribute("fullname").toString();
			String usertype = session.getAttribute("usertype").toString();
			String message = "";
			
			if(usertype.equals("Applicant"))
				message = "Welcome applicant. You may now start looking for your career opportunity.";
			else
				message = "Welcome employer. You may now start browsing applicant profiles.";	
		%>
		
		
		<h1> Welcome <%= name %>! </h1>
		
		<p> <%= message %></p>
		
		
		
	</body>
</html>