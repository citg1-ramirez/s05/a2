<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title> Servlet Job Finder (Ramirez)</title> 
	</head>
	
	<body>
		<h1> Welcome to Servlet Job Finder! </h1> <br>

		<form action="register" method="post">
			<label for= "firstname"> First Name: </label>
				<input type= "text" name="firstname" required> <br> <br>
			
			
			<label for= "lastname"> Last Name: </label>
				<input type= "text" name="lastname" required> <br> <br>
			
			
			<label for= "phonenumber"> Phone Number: </label>
				<input type= "text" name="phonenumber" required> <br> <br>
		
		
			<label for= "emailaddress"> Email Address: </label>
				<input type= "email" name="emailaddress" required> <br> <br>
		
		
			<fieldset style="display: inline-block; width: 500px;">
				<legend> How did you discover the job finder application? </legend>
					<input type="radio" name="discover" id="discover_friends" value="friends">
						<label for="discover_friends"> Friends </label> <br>
						
					<input type="radio" name="discover" id="disocver_socialmedia" value="socialmedia">
						<label for="disocver_socialmedia"> Social Media </label> <br>
						
					<input type="radio" name="discover" id="disocver_others" value="others">
						<label for="disocver_others"> Others </label> <br>
			</fieldset> <br> <br>
			
			
			
			<label for="birthdate"> Date of Birth: </label>
				<input type="date" name="birthdate" required> <br> <br>
				
				
			<label for="usertype"> Are you an <b>employer</b>  or an <b>applicant</b>? </label>
				<select id="usertype" name="usertype" required >
					
					<option value="" selected="selected"> Select one. </option>
					
					<option value="Applicant"> Applicant </option>
					
					<option value="Employer"> Employer </option>
					
				</select> <br> <br>
				
				
			<label for="description"> Profile Description: </label>
				<textarea name="description" maxlength="300"></textarea> <br> <br>
			
			
			<button> Register </button>
			
		</form>
	</body>
</html>