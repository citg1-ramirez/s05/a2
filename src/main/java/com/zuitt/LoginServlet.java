package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;

	
	public void init() throws ServletException {
		System.out.println("----------------------------------------");
		System.out.println("Login Servlet connection started.");
		System.out.println("----------------------------------------");
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		HttpSession session = req.getSession();
		
		String fullname = session.getAttribute("firstname").toString() +  " " + session.getAttribute("lastname").toString();;
		String usertype = session.getAttribute("usertype").toString();
		
		
		session.setAttribute("fullname", fullname);
		session.setAttribute("usertype", usertype);	
		
		
		res.sendRedirect("home.jsp");
	}
	
	
	public void destroy() {
		System.out.println("----------------------------------------");
		System.out.println("Login Servlet connection destroyed.");
		System.out.println("----------------------------------------");
	}

}
