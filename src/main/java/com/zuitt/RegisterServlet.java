package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;

	public void init() throws ServletException {
		System.out.println("----------------------------------------");
		System.out.println("Register Servlet connection started.");
		System.out.println("----------------------------------------");
	}

	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String phonenumber = req.getParameter("phonenumber");
		String emailaddress = req.getParameter("emailaddress");
		String discover = req.getParameter("discover");
		String birthdate = req.getParameter("birthdate");
		String usertype = req.getParameter("usertype");
		String description = req.getParameter("description");

		HttpSession session = req.getSession();
		
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phonenumber", phonenumber);
		session.setAttribute("emailaddress", emailaddress);
		session.setAttribute("discover", discover);
		session.setAttribute("birthdate", birthdate);
		session.setAttribute("usertype", usertype);
		session.setAttribute("description", description);
	
		res.sendRedirect("register.jsp");
		
	}

	
	
	public void destroy() {
		System.out.println("----------------------------------------");
		System.out.println("Register Servlet connection destroyed.");
		System.out.println("----------------------------------------");
	}
}
